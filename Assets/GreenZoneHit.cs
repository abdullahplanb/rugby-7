﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenZoneHit : MonoBehaviour {
    public delegate void GreenZone();
    public event GreenZone GreenZoneHitEvent;

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball")
        {
            GreenZoneHitEvent();
        }
    }
}
