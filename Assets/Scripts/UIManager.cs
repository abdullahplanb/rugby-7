﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager Instance { get; private set; }

    public Button nextButton;
    public Text scoretxt;
    public GameObject frontCanvas;
    public Text Awesome;



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        SubscribeListeners();
    }



    // Use this for initialization
    void Start () {
        //SubscribeListeners();
    }

    void GameOver() {
        scoretxt.text = "Score: " + GameController.Instance.score.ToString();
        frontCanvas.SetActive(true);
        nextButton.interactable = true;
        //UnSubscribeListeners();
        
    }

    void GreenZoneHit() {
        print("well shot");
    }

    void NextGame()
    {
        nextButton.interactable = false;
        frontCanvas.SetActive(false);
        print("welcome");
    }

    void SubscribeListeners()
    {
        GameController.Instance.GameOverEvent += GameOver;
        GameController.Instance.NextGameEvent += NextGame;
        GameObject.FindObjectOfType<GreenZoneHit>().GreenZoneHitEvent += GreenZoneHit;
    }

}
