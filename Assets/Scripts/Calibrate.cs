﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Calibrate : MonoBehaviour {
    public Transform foot;
    public Transform shoes;

    public GameObject CalibrationCanvas;
    public Button CalibrateBtn;

    public Text timer;

    Vector3 shoesPos;
    Quaternion shoesRot;
	// Use this for initialization
	void Start () {
        //StartCalibration();
        shoesPos = shoes.position;
        shoesRot = shoes.rotation;
    }

    public void StartCalibration() {
        CalibrationCanvas.SetActive(true);
        CalibrateBtn.interactable = false;
        StartCoroutine(Timer());

        shoes.position = shoesPos;
        shoes.rotation = shoesRot;
        shoes.parent = null;
    }

    IEnumerator Timer() {
        float time = 10.0f;
        while(time > 0)
        {
            time -= Time.deltaTime;
            timer.text = time.ToString("F0") + " sec"; 
            yield return null;
        }
        shoes.parent = foot;
        CalibrationCanvas.SetActive(false);
        CalibrateBtn.interactable = true;
    }

    public void Recenter() {
        Valve.VR.OpenVR.System.ResetSeatedZeroPose();
        Valve.VR.OpenVR.Compositor.SetTrackingSpace(
        Valve.VR.ETrackingUniverseOrigin.TrackingUniverseSeated);
    }
}
