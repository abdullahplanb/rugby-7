﻿using UnityEngine;

public class Kick : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "foot")
        {
            GameController.Instance.PlayerKicked(collision);
        }
    }

}
