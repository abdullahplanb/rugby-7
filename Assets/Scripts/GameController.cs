﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public delegate void nextGame();
    public event nextGame NextGameEvent;

    public delegate void gameOver();
    public event gameOver GameOverEvent;

    public GameObject RugbyBall;
    public float kickPower = 2.3f;
    public float resetWait = 5.0f;
    public Shoes foot;
    public int chances;
    [HideInInspector]
    public int score;

    Rigidbody rb;
    Vector3 initialPos;
    Quaternion initialRot;
    Vector3 requiredVel;

    public float forwardMul;

    bool IsGameOver;

    public static GameController Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        Initialize();
        NextGame();
    }

    public void PlayerKicked(Collision collision)
    {
        if (!IsGameOver)
        {
            requiredVel = collision.contacts[0].normal + new Vector3(0, 0.5f, 0) + (collision.transform.forward * forwardMul);
            rb.AddForceAtPosition(requiredVel * foot.speed * kickPower, collision.contacts[0].point, ForceMode.Impulse);
            Invoke("ResetBall", resetWait);
            chances--;
        }
    }

    public void ResetBall()
    {
        rb.isKinematic = true;
        RugbyBall.transform.position = initialPos;
        RugbyBall.transform.rotation = initialRot;

        if (chances <= 0)
            GameOver();
        else
            rb.isKinematic = false;
    }

    void GameOver() {
        rb.isKinematic = true;
        IsGameOver = true;
        if(GameOverEvent != null)
        {
            GameOverEvent();
        }
        print("score: " + score);
    }

    public void NextGame()
    {
        IsGameOver = false;
        score = 0;
        chances = 3;
        ResetBall();
        if (NextGameEvent != null)
        {
            NextGameEvent();
        }

    }

    public void GreenZoneHit()
    {
        score += 500;
    }

    private void Initialize()
    {
        initialPos = RugbyBall.transform.position;
        initialRot = RugbyBall.transform.rotation;
        rb = RugbyBall.GetComponent<Rigidbody>();
        GameObject.FindObjectOfType<GreenZoneHit>().GreenZoneHitEvent += GreenZoneHit;
    }
}
