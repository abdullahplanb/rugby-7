﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerCheck : MonoBehaviour {

	public UnityEvent TriggerEnter;
	public UnityEvent TriggerExit;

	public string TagToCompare = "Player";

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == TagToCompare) {
			TriggerEnter.Invoke ();
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == TagToCompare) {
			TriggerExit.Invoke ();
		}
	}
}
