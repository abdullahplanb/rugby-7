﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoes : MonoBehaviour {
    [HideInInspector]
    public float speed = 0;
    Vector3 oldPos;

	// Use this for initialization
	void Start () {
        oldPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        speed = (transform.position - oldPos).magnitude / Time.deltaTime;
        oldPos = transform.position;
    }
}
